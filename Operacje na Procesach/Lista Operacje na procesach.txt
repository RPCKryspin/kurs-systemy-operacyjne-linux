Zad.1. (2 punkty - na zajęciach)
Zapoznaj się z programem ps (man ps). Wypróbuj dwa formaty wyświetlania rozszerzonych informacji: -f i -l. Przeczytaj podręcznik polecenia ps aby zrozumieć wyświetlane parametry. Naucz się wybierać zestaw procesów do wyświetlania argumentami opcjonalnymi -u, -t, i -p.

Ile Twoich procesów istniało w systemie w chwili przykładowego wywołania ps?

Ile procesów sshd istniało w systemie (serwerze) w chwili wykonywania ćwiczenia (sprawdź polecenie pgrep)?

Zad.2. (2 punkty - na zajęciach)
W jednym okienku terminala uruchom hierarchię co najmniej trzech procesów, tzn. proces "A", który uruchomi proces potomny "B", który uruchomi proces potomny "C". Mogą to być kolejno uruchamiane interpretery poleceń, albo uruchamiające się kolejno skrypty lub programy. W innym oknie terminala sprawdź programem ps zależności potomków i rodziców w utworzonej hierarchii.

Poleceniem kill zabij proces w środku tej hierarchii ("B"). Sprawdź poleceniem ps co pozostało z pierwotnych procesów, i czy osierocony proces zostanie poprawnie adoptowany przez proces init.

Zad.3. (2 punkty - na zajęciach, lub 1 punkt - w domu)
Napisz skrypt, który w pętli będzie coś robił (np. co sekundę wypisywał bieżącą godzinę). Sprawdź wysyłanie do procesu różnych sygnałów poleceniem kill (SIGINT, SIGQUIT, ale także SIGFPE, SIGILL). Następnie rozbuduj skrypt o przechwytywanie tych sygnałów (trap) i sprawdź, że to działa, to znaczy, że tak napisanego procesu nie da się zabić tymi sygnałami. Sprawdź możliwość uśmiercenia sygnałem SIGKILL procesu, który przechwytuje wszystkie 15 podstawowych sygnałów.

Uwaga: trap jest wbudowanym poleceniem shella, i jego wywołanie trochę się różni pod różnymi shellami. Najłatwiej jest wykonać to ćwiczenie przy użyciu Bourne shella (/bin/sh), ponieważ bash wykonuje różne potajemne zabiegi z obsługą sygnałów. Jednak polecenie trap w Bourne shellu jest prymitywne - czytaj man sh.

Zad.4. (2 punkty - na zajęciach, lub 1 punkt - w domu)
Sprawdź możliwość zawieszania procesu sygnałem SIGSTOP i wznawiania sygnałem SIGCONT. Sprawdź, że sygnał SIGSTOP daje taki sam efekt jak naciśnięcie klawisza Ctrl+Z na terminalu. Sprawdź, że sygnał SIGCONT daje taki sam efekt jak wykonanie polecenia fg lub bg (którego bardziej?).

Zad.5. (2 punkty - na zajęciach, lub 1 punkt - w domu)
Uruchom potok co najmniej trzech poleceń działających przez jakiś zauważalny czas. Mogą to być odpowiednio dobrane polecenia systemowe, lub samodzielnie napisane skrypty. Sprawdź poleceniem ps i odpowiedz jakie zachodzi pokrewieństwo między tymi procesami (jeśli w ogóle zachodzi).

Wskazówka: ponieważ potok służy do przesyłania danych od procesu do procesu, i synchronizuje pracę wszystkich procesów odpowiednio do pojawiających się danych, dobrą metodą generowania demonstracyjnego potoku jest umieszczenie na jego początku procesu, wysyłającego na swoje wyjście stały strumień danych, np. piszącego w pętli co sekundę jakiś krótki komunikat. Kolejne elementy potoku mogą być realizowane przez program cat.

W raporcie odpowiedz na postawione w zadaniu pytanie.

Zad.6. (2 punkty - w domu)
Poleceniem mknod utwórz potok nazwany (FIFO). W dwóch różnych okienkach uruchom odczytywanie zawartości potoku (np. poleceniem cat), a w jeszcze innym okienku uruchom zapisywanie do tego samego potoku.

Możesz w tym również wykorzystać cat i wpisywać ręcznie dowolne teksty z linijkami różnej długości, albo możesz znaleźć lub samodzielnie napisać jakiś program wypisujący na wyjściu takie teksty.

Sprawdź (i podaj w raporcie) jak system rozdziela dane z potoku dwóm procesom jednocześnie usiłującym z niego czytać. Podsumuj w raporcie. Opisz również zastosowany sposób generowania tekstów.

Zabijając na przemian procesy piszące/czytające, zaobserwuj, kiedy proces cat czytający z potoku czeka na więcej danych, a kiedy kończy pracę. Analogicznie, kiedy proces piszący czeka w gotowości do dalszego pisania na potoku, a kiedy samoistnie kończy pracę. Podsumuj w raporcie.

Zad.7. (2 punkty - w domu)
Sprawdź wartości priorytetów procesów i ich liczby nice, a następnie przećwicz obniżanie priorytetu pracującego w tle procesu (nice/renice). Czy potrafisz zademonstrować działanie obniżonego liczbą nice priorytetu?

Wskazówka: skutki obniżenia priorytetu łatwiej jest zaobserwować na starszej maszynie, z mniejszą liczbą procesorów/rdzeni. Spróbuj wykonać taki eksperyment na diablo, który ma tylko 8 fizycznych procesorów. Uruchom na nim silnie obliczeniowy proces np. w 10 (lub więcej) instancjach, i jednej z nich obniż priorytet. Obserwuj zużycie czasu cpu przez procesy. W tym celu przydaje się program top (naucz się ograniczyć go tylko do własnych procesów). Znajdź czas na wykonanie takiego eksperymentu kiedy podobnych działań nie wykonują inni. Ogranicz do minimum czas nadmiernego obciążania tej maszyny, i koniecznie posprzątaj po sobie - zakończ wszystkie testowo uruchomione procesy obliczeniowe.

W raporcie zwięźle opisz wykonane działania i otrzymane wyniki. Podaj polecenie obniżania priorytetu procesu.

Zad.8. (2 punkty - w domu)
Zapoznaj się z poleceniem ulimit. Na początek ustaw ograniczenie liczby procesów na 2 (ulimit -u 2) i uruchom dowolny trywialny program, np. ls. Jaki jest wynik? Jak rozwiązać powstały problem?

Następnie ustaw limit procesów na nieco większą liczbę i napisz skrypt, który w nieskończonej pętli uruchamia kolejne kopie samego siebie w tle. Uruchom skrypt koniecznie w tej powłoce, w której ograniczyłeś/ograniczyłaś maksymalną liczbę procesów! Następnie spróbuj opanować sytuację, czyli pozabijać wszystkie utworzone procesy i powrócić do normalnej pracy.