#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main()
{

	char imagename[100];
	char imagespace[] = "ss.png";
	int descriptor[2]; // deskryptor
	void *pointer;
	struct stat filestat;
	pid_t PID = fork();
	
	if(PID < 0)
	{
		printf("Nie utworzono procesu potomnego\n");
		return 1;
	}
	if(PID == 0)
	{
		execlp("display","display","-update","1",imagespace,NULL);
	}
	if(PID > 0)
	{
		while(1)
		{
			printf("Podaj nazwe obrazka:\t");
			scanf("%s",imagename);
			descriptor[0] = open(imagename, O_RDONLY);
			while(descriptor[0] < 0)
			{
				printf("name error\n");
				printf("Podaj nazwe obrazka:\t");
				scanf("%s",imagename);
				descriptor[0] = open(imagename, O_RDONLY);
			}

			stat(imagename, &filestat);

			descriptor[1] = open(imagespace, O_RDWR);

			truncate(imagespace,filestat.st_size);
			pointer = mmap(0, filestat.st_size,PROT_WRITE | PROT_READ, MAP_SHARED, descriptor[1], 0);

			read(descriptor[0], pointer, filestat.st_size);
			msync(pointer, filestat.st_size,MS_SYNC);
			munmap(pointer, filestat.st_size);
			close(descriptor[0]);
			close(descriptor[1]);
		}
	}
	
}

