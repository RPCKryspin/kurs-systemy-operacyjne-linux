#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main()
{

	char filenametab[100];
	char filespace[]="text.txt";
	int descriptor[2]; // deskryptory
	char *pointer;
	struct stat filestat;
	
	while(1)
	{
		descriptor[1]=open(filespace,O_RDWR,0);
		if(descriptor[1]<0)	
			printf("Write error");

		printf("Podaj nazwe pliku\n");
		scanf("%s",filenametab);

		descriptor[0]=open(filenametab,O_RDONLY,0);
		if(descriptor[0]<0)
			printf("No file\n");
		else
		{
			stat(filenametab,&filestat);
			truncate(filespace,filestat.st_size);
			pointer = mmap(0, filestat.st_size,PROT_WRITE | PROT_READ, MAP_SHARED, descriptor[1], 0);
			read(descriptor[0],pointer,filestat.st_size);
			msync(pointer,filestat.st_size,MS_SYNC);
			munmap(pointer, filestat.st_size);
			close(descriptor[0]);
			
		}
		close(descriptor[1]);
	}
}
