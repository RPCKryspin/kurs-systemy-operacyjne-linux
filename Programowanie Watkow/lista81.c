#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void* Wypisz(void* ID)
{
	long IDL = (long)ID;
	printf("Hello SCR. Written by thread ID#%ld\n",IDL+1);
	pthread_exit(0);
}

int main(int argc, char **argv)
{
	long Liczba_Watkow;
	printf("Podaj ilość watków do utworzenia: ");
	scanf("%ld",&Liczba_Watkow);
	pthread_t Watek_cykl[Liczba_Watkow];

	long i = 0;
	while(i < Liczba_Watkow)
	{
		printf("Tworze Watek ID#%ld\n",i+1);
		pthread_create(&Watek_cykl[i],0,Wypisz, (void *)i);
		i++;
	}

	long j = 0;
	while(j < Liczba_Watkow) // Cykl pracy
	{
		pthread_join(Watek_cykl[j], 0);
		printf("Watek ID#%ld zakonczony\n",j+1);
		j++;
	}

	pthread_exit(0);
}
