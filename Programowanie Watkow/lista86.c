#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define LICZBA_WATKOW 10

pthread_mutex_t PI_mutex;
double PI;

struct thread_data
{
   int	thread_id;
   long double myPI;
   double mytime;

};

struct thread_data thread_data_array[LICZBA_WATKOW];

double get_actual_time_in_us()
{
    struct timespec now;
    clock_gettime(TIME_UTC, &now);
    return now.tv_sec*1e6 + now.tv_nsec*1e-3;
}

void *Count_PI(void *ID)
{
	int IDI = (int)ID;
	int r = 1; //promien
	long double Pole_Kwadrat = 4*r*r;
	int pkt = (Pole_Kwadrat * 2500);
	int Kolo_pkt = 0; //liczba punktów w kole
	int Kwadrat_pkt = 0; //liczba punktów w kwadracie
	double x,y;

	int i = 1;
	while(i <= pkt) // wylosuj punkt,
	{
		y = drand48();
		x = drand48();
		if( (x*x + y*y) <= r*r )
		{ 
			Kolo_pkt++;
		}
		Kwadrat_pkt++;
		i++;
	}

	long double ratio = (long double)Kolo_pkt/Kwadrat_pkt;
	pthread_mutex_lock(&PI_mutex);
	PI= 4*ratio;
	thread_data_array[IDI].myPI = PI;
	pthread_mutex_unlock(&PI_mutex);
}

int main()
{	
	printf("\n");
	pthread_t Cykl[LICZBA_WATKOW];
	double Estymata =0;
	double min=0, max=0;
	pthread_mutex_init(&PI_mutex, 0);

  	

	int t=0;
	while(t<LICZBA_WATKOW) 
	{
		thread_data_array[t].thread_id = t;
		double time_start = get_actual_time_in_us();
		printf("Tworze Watek ID#%d\n", t);

		int Bezpiecznik = pthread_create(&Cykl[t], 0, Count_PI, (void *)thread_data_array[t].thread_id);
		if (Bezpiecznik) 
		{
			printf("pthread_create() error ID#%d\n", Bezpiecznik);
			exit(-1);
		}
		double time_stop = get_actual_time_in_us();
		thread_data_array[t].mytime =time_stop-time_start;
		t++;
	}

	printf("\n");
	
	int j=0;
	while(j < LICZBA_WATKOW)
	{
		pthread_join(Cykl[j], 0);
		j++;
	}

	int i=0;
	while(i < LICZBA_WATKOW)
	{
		printf("Watek ID#%d, Wartosc PI=%Lf\n",thread_data_array[i].thread_id,thread_data_array[i].myPI);
		printf ("It took %.3lf microseconds to run.\n\n", thread_data_array[i].mytime  );
		Estymata+=thread_data_array[i].myPI;

		if((thread_data_array[i+1].myPI>thread_data_array[i].myPI) && (i!=9))
			max = thread_data_array[i+1].myPI;
		if((thread_data_array[i+1].myPI<thread_data_array[i].myPI) && (i!=9))
			min = thread_data_array[i+1].myPI;
		i++;
	}

	pthread_mutex_destroy(&PI_mutex);
	printf("\nEstymata PI=%lf\n\n",(Estymata/LICZBA_WATKOW));
	printf("\nMinimum PI=%lf\n\n",min);
	printf("\nMaximum PI=%lf\n\n",max);
	exit(0);
}

