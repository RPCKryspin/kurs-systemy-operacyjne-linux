#include <stdio.h>
#include <stdlib.h>

typedef struct list // Lista Jednokierunkowa
{
   int numer; //numer procesu 
   int priorytet;
   int czas; //czas roboty
   struct list* next;
}list_type;

void list_init(list_type **head) //funkcje listy
{
   *head = (list_type *)malloc(sizeof(list_type));
   *head = NULL;
}

void add_to_list(list_type **head, int numer, int priorytet, int czas) //funkcje listy
{
   if(*head == NULL)
   {
       *head = (list_type *)malloc(sizeof(list_type));
      (*head) -> numer = numer;
      (*head) -> priorytet = priorytet;
      (*head) -> czas = czas;
   }
   else
   {
      list_type *polozenie= *head;

      while(polozenie -> next != NULL)
      {
         polozenie = polozenie -> next;
      }

      polozenie -> next = (list_type *)malloc(sizeof(list_type));
      polozenie -> next -> numer = numer;
      polozenie -> next -> priorytet = priorytet;
      polozenie -> next -> czas = czas;
      polozenie -> next -> next = NULL;
   }
}

void remove_from_list(list_type **head, int indeks) //funkcje listy
{
   list_type *polozenie = *head;
   list_type *temporary;

   if(indeks == 1)
   {
      *head = polozenie -> next;
       free(polozenie);
   }
   else
   {
      int i=1;
      while(polozenie->next != NULL && i<indeks-1)
      {
         polozenie = polozenie -> next;
         i++;
      }

      temporary = polozenie -> next;
      polozenie -> next = temporary -> next;
      free(temporary);
   }
}

int** processor_init(int **processor, int processor_liczba) // rezerwacja pamieci na procesory
{
   processor = malloc(processor_liczba * sizeof(int*));
   for(int i=0; i<processor_liczba; i++)
   {
      processor[i] = malloc(2 * sizeof(int));
      processor[i][0] = -1;
      processor[i][1] = 0;
   }
   return processor;
}

void sjf(list_type **kolejka, int **processor, int processor_liczba)
{
   int next_processor = 0;

   for(int i=0; i<processor_liczba; i++) //sprawdzanie procesorow w akcji
   {
      if(processor[i][0] != -1)
      {
         processor[i][1]--;

         if(processor[i][1] < 1)
            processor[i][0] = -1;
      }
   }

   while(next_processor < processor_liczba && processor[next_processor][0] != -1) // pierwszy wolny procesor
   {
      next_processor++;
   }

   for(int i=1; *kolejka!=NULL && next_processor!=processor_liczba; i+=3) // przydzielanie zadan
   {
	list_type *kolejka_1 = *kolejka;
	int min = kolejka_1->czas;
	int nr=kolejka_1->numer;
	int kolejka_nr=1;

	for(int g=1;kolejka_1!=NULL;g++)
	{

		if(kolejka_1->czas<min)
		{
			min=kolejka_1->czas; kolejka_nr=g; nr=kolejka_1->numer;
		}
		kolejka_1=kolejka_1->next;
		
	}
      processor[next_processor][0] = nr;
      processor[next_processor][1] = min;
      remove_from_list(kolejka,kolejka_nr);

      while(next_processor < processor_liczba && processor[next_processor][0] != -1)
      {
         next_processor++;
      }
   }
}

void srtf(list_type **kolejka, list_type **process, int **processor, int processor_liczba)
{
   int next_processor = 0;

   for(int i=0; i<processor_liczba; i++) //sprawdzanie procesorow w akcji
   {
      if(processor[i][0] != -1)
      {
         processor[i][1]--;

         if(processor[i][1] < 1)
            processor[i][0] = -1;
      }
   }

   if(*kolejka!=NULL)
   {
	while((*process)!=NULL)
	{
		int prc=0;
		while(processor[prc][0] != (*process)->numer) 
		{
            if(prc == processor_liczba - 1) break; prc++; // Wazne linia antysegmentowa #lack of processors
        }
		add_to_list(kolejka,processor[prc][0],(*process)->priorytet,processor[prc][1]);
		remove_from_list(process,1);
		processor[prc][0]=-1;
	}
   }
   while(next_processor < processor_liczba && processor[next_processor][0] != -1) // pierwszy wolny procesor
   {
      next_processor++;
   }

   for(int i=1; *kolejka!=NULL && next_processor!=processor_liczba; i+=3) // przydzielanie zadan
   {
	list_type *kolejka_1 = *kolejka;
	int min = kolejka_1->czas;
	int nr=kolejka_1->numer;
	int kolejka_nr=1;

	for(int g=1;kolejka_1!=NULL;g++)
	{

		if(kolejka_1->czas<min)
		{
			min=kolejka_1->czas; kolejka_nr=g; nr=kolejka_1->numer;
		}
		kolejka_1=kolejka_1->next;
		
	}
      processor[next_processor][0] = nr;
      processor[next_processor][1] = min;
      add_to_list(process,nr,1,min);
      remove_from_list(kolejka,kolejka_nr);

      while(next_processor < processor_liczba && processor[next_processor][0] != -1)
      {
         next_processor++;
      }
   }
}

int main (int argc, char *argv[])
{
/* ARG na strumien
1 - kod strategii (1 - sjf, 2 - srtf)
2 - liczba procesorow
3 - wilkosc kwantu czasu - wywlaszczanie
*/
   int **processor; // dynamiczna tablica procesorów
   int kwant = 1; //kwant czasu
   int time = 0; //czas lokalny
   int strategia;
   int pozycja;
   int resztki; 
   int paramrametry[3]; 
   char pamiec[255] ; // wczytywana ilosc pliku
   char znak; // znak porzadkowy
   int processor_liczba; // liczba procesorów
   list_type *kolejka ; // kolejka procesow oczekujacych
   list_type *process; // procesy wlasciwosci
   list_init(&kolejka);
   list_init(&process);

   strategia = argv[1][0] - 48;  // znak ASCII
   if(argc > 2)
      processor_liczba = argv[2][0] - 48;
   else
      processor_liczba = 1;
   if(argc > 3)
      kwant = argv[3][0] - 48;
   processor = processor_init(processor, processor_liczba);

   while((znak = getchar()) != EOF) // czytanie do tablicy
   {
      pozycja = 1;
      pamiec[0] = znak;
      while( (znak = getchar()) != '\n')
      {
         pamiec[pozycja] = znak;
         pozycja++;
      }
      pamiec[pozycja] = '\0';

      int j = 2;
      while(pamiec[j-1] != '\0') //wrzucanie nowych procesow do kolejki procesow
      { 
         for(int i = 0; i<3; i++)
         {
            paramrametry[i] = 0;
            for(j; pamiec[j] != ' ' && pamiec[j] != '\0'; j++)
            {
               paramrametry[i] = paramrametry[i] * 10 + pamiec[j] - 48; // konwersja do int
            }
            j++;
         }
         add_to_list(&kolejka, paramrametry[0], paramrametry[1], paramrametry[2]);
      }

      if(strategia == 1)
         sjf(&kolejka, processor, processor_liczba);
      if(strategia == 2)
         srtf(&kolejka, &process, processor, processor_liczba);

      printf("%d",time);
      for(int i=0; i<processor_liczba; i++)
      {
         printf(" %d",processor[i][0]);
      }
      printf("\n");
      time++;
   }

   resztki = 0;
   for(int i=0; i<processor_liczba; i++)
   {
      if(processor[i][0] != -1)
         resztki = 1;
   }
   while(resztki)  // Szeregowanie resztek procesow
   {
      if(strategia == 1)
         sjf(&kolejka, processor, processor_liczba);
      if(strategia == 2)
         srtf(&kolejka, &process, processor, processor_liczba);

      resztki = 0;
      for(int i=0; i<processor_liczba; i++)
      {
         if(processor[i][0] != -1)
            resztki = 1;
      }

      printf("%d",time);
      for(int i=0; i<processor_liczba; i++)
      {
         printf(" %d",processor[i][0]);
      }
      printf("\n");
      time++;
   }

   return 0;
}
