#include <stdio.h>
#include <string.h>
#include <unistd.h> // zaplecze pipe fork close etc
#include <fcntl.h> // zaplecze obslugi plikow funkcji I/O open  

#define LENGHT 16

int main(int argc,char *argv[])
{

	int pipetab[2];
	int filetab;
	int licz_znaki;
	char chartab[LENGHT];

	if (pipe(pipetab) == -1)
	{
		fprintf(stderr,"Potok Error \n");
		return -1;
	}

	pid_t PID = fork();

	if(PID <0)
	{
		fprintf(stderr,"Process Error\n");
		return -1;
	}

	else if(PID==0)
	{
		close(pipetab[1]);

		while((licz_znaki=read(pipetab[0],chartab,LENGHT))>0)
		{
			write(1," #",2*sizeof(char))<0?fprintf(stderr,"write error"):0;
			write(1,chartab,licz_znaki)<0?fprintf(stderr,"write error"):0;
			write(1,"# ",2*sizeof(char))<0?fprintf(stderr,"write error"):0;
		}

		close(pipetab[0]);
	}

	else
	{
		close(pipetab[0]);

		if((filetab=open(argv[1],O_RDONLY)) < 0)
		{
			fprintf(stderr,"open error\n");
			return -1;
		}

		while((licz_znaki=read(filetab,chartab,LENGHT))>0)
		{
			if(write(pipetab[1],chartab,licz_znaki)<0)
				fprintf(stderr,"write error");
		}
		close(pipetab[1]);
	}
}
