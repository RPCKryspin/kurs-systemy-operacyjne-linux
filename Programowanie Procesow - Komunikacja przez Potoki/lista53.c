#include <stdio.h>
#include <string.h>
#include <unistd.h> // zaplecze pipe fork close etc
#include <fcntl.h> // zaplecze obslugi plikow funkcji I/O open  
#include <errno.h>
#include <limits.h>

#define FIFOPIPE "pipe_tmp"
#define LENGHT 255
#define SIZE 16

int main(int argc,char *argv[])
{
	int pipetab;
	int filetab;
	int licz_znaki;
	char chartab[LENGHT];

	int text = argc; // argument
	if((pipetab=open(FIFOPIPE,O_WRONLY)) == -1)
	{
		fprintf(stderr,"Potok Error \n");
		return -1;
	}

	while(--text>0)
	{
		if((filetab=open(argv[text],O_RDONLY)) < 0)
		{
			fprintf(stderr,"open error \n");
			return -1;
		}
		while((licz_znaki=read(filetab,chartab,SIZE))>0)
		{
			if(write(pipetab,chartab,licz_znaki)<0)
				fprintf(stderr,"write error\n");
		}
		fprintf(stdout,"Queued - waiting\n");
		sleep(5);
	}
	close(pipetab);
}
