#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <ucontext.h>
struct timespec t;
void sigterm_hand(int sygnal)
{
  printf("Sygnal term - koniec informacji\n");
}

void sigusr1_hand(int sygnal)
{
  printf("Sygnal usr1 - Ignorowanie\n");
  signal(SIGUSR1,SIG_IGN);
}

void sigusr2_hand(int sygnal)
{
  for(int i=0; i<1000; i++)
  {
    printf("Sygnal usr2 - ignoruj 1000 iteracji \n");
    signal(SIGUSR2,SIG_IGN);
    nanosleep(&t,0);
  }
}

void sigalrm_hand(int sygnal)
{
  printf("Sygnal alrm - koniec programu \n");
  exit(1);
}

int main()
{
  long int i=0;
  t.tv_sec=0;
  t.tv_nsec=100;

  signal(SIGTERM,sigterm_hand);
  signal(SIGUSR1,sigusr1_hand);
  signal(SIGUSR2,sigusr2_hand);
  signal(SIGALRM,sigalrm_hand);

  while(1)
    {
      nanosleep(&t,0);
      ++i;
    }
}
